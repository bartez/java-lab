package com.bartez.lab7.dialog;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.support.v4.app.DialogFragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bartez.lab7.R;
import com.bartez.lab7.model.Person;
import com.mukesh.countrypicker.fragments.CountryPicker;
import com.mukesh.countrypicker.interfaces.CountryPickerListener;
public class PersonFormDialog extends DialogFragment{

    public static interface OnCompleteListener {
        public abstract void onComplete(Person person);
    }

    private OnCompleteListener mListener;
    final private Person newPerson = new Person();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.new_person_dialog, null);
        builder.setView(view);

        final Spinner sexSpinner = (Spinner)view.findViewById(R.id.person_form_sex_choice) ;
        final Button countryButton = (Button)view.findViewById(R.id.choose_country_btn);
        final TextView countryLabel = (TextView)view.findViewById(R.id.choose_country_label);
        final CountryPicker picker = CountryPicker.newInstance(getString(R.string.select_country));

        picker.setListener(new CountryPickerListener() {
            @Override
            public void onSelectCountry(String name, String code, String dialCode, int flagDrawableResId) {
                newPerson.setCountryISO2(code);
                countryLabel.setText(String.format(getString(R.string.country_selected), name));
                picker.dismiss();
            }
        });

        countryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                picker.show(getFragmentManager(), "COUNTRY_PICKER");
            }
        });

        ArrayAdapter sexAdapter = ArrayAdapter.createFromResource(view.getContext(),R.array.sex,
                R.layout.support_simple_spinner_dropdown_item);

        sexSpinner.setAdapter(sexAdapter);

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                EditText nameText = (EditText) getDialog().findViewById(R.id.person_form_name);
                EditText surnameText = (EditText) getDialog().findViewById(R.id.person_form_surname);
                EditText phoneText = (EditText) getDialog().findViewById(R.id.person_form_tel);
                newPerson.setName(nameText.getText().toString());
                newPerson.setSurname(surnameText.getText().toString());
                newPerson.setPhoneNumber(phoneText.getText().toString());
                newPerson.setMan(sexSpinner.getSelectedItem().toString().toLowerCase().equals("man"));
                mListener.onComplete(newPerson);
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                PersonFormDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnCompleteListener)activity;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }
}

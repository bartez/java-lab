package com.bartez.lab7;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.bartez.lab7.adapter.PersonAdapter;
import com.bartez.lab7.dialog.PersonFormDialog;
import com.bartez.lab7.model.Person;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity implements PersonFormDialog.OnCompleteListener {

    private List<Person> persons = new ArrayList<>();
    PersonFormDialog dialog = new PersonFormDialog();
    private final int newPersonCode = 1;
    private PersonAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        seedExampleData();

        adapter = new PersonAdapter(getApplicationContext(), persons);
        ListView personView = (ListView) findViewById(R.id.person_list);
        personView.setAdapter(adapter);
        Button createNewPersonBtn = (Button)findViewById(R.id.add_new_person_btn);
        createNewPersonBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show(getSupportFragmentManager(), "NEW_PERSON_FRAGMENT");
            }
        });
    }

    private void seedExampleData(){
        Person p1 = new Person();
        p1.setName("Adam");
        p1.setMan(true);
        p1.setPhoneNumber("+48 444 233");
        p1.setSurname("Małysz");
        p1.setCountryISO2("PL");
        Person p2 = new Person();
        p2.setName("Jahne");
        p2.setMan(true);
        p2.setPhoneNumber("+33 233 2136");
        p2.setSurname("Ahonen");
        p2.setCountryISO2("FI");

        Person p3 = new Person();
        p3.setName("Justyna");
        p3.setPhoneNumber("+48 424 231");
        p3.setSurname("Kowalczyk");
        p3.setCountryISO2("PL");

        persons.add(p1);
        persons.add(p2);
        persons.add(p3);
    }

    @Override
    public void onComplete(Person person) {
        if(person != null){
            persons.add(person);
            adapter.notifyDataSetChanged();
        }
    }
}

package com.bartez.lab7.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bartez.lab7.R;
import com.bartez.lab7.model.Person;

import java.util.ArrayList;
import java.util.List;

public class PersonAdapter extends BaseAdapter {

    private Context context;
    private List<Person> persons = new ArrayList<>();
    private static LayoutInflater inflater;

    public PersonAdapter(Context context, List<Person> persons){
        this.context = context;
        this.persons = persons;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }
    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int i) {
        return persons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if(view == null){
            view = inflater.inflate(R.layout.person_row_layout, viewGroup, false);
        }

        Person person = persons.get(i);

        ImageView personImage = (ImageView)view.findViewById(R.id.person_img);
        ImageView personImageCountry = (ImageView)view.findViewById(R.id.person_img_country);
        TextView personName = (TextView)view.findViewById(R.id.person_name);
        TextView personTel = (TextView)view.findViewById(R.id.person_tel);
        TextView personCountry = (TextView)view.findViewById(R.id.person_country);
        Button personRemoveButton = (Button)view.findViewById(R.id.person_removeBtn);

        personRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                persons.remove(i);
                Toast.makeText(context, "Person removed!", Toast.LENGTH_SHORT).show();
                notifyDataSetChanged();
            }
        });

        personName.setText(person.getName() + " " + person.getSurname());
        personTel.setText(person.getPhoneNumber());
        personCountry.setText(person.getCountry());
        personImage.setImageResource(person.isMan() ? R.drawable.man : R.drawable.woman);

        if(person.getCountryISO2() != null){
            int flagId = context.getResources().getIdentifier("flag_" + person.getCountryISO2().toLowerCase(), "drawable", context.getPackageName());
            personImageCountry.setImageResource(flagId);
        }

        return view;
    }
}

package com.bartez.lab7.model;

import java.util.Locale;

public class Person {

    private String name;
    private String surname;
    private boolean isMan;
    private String phoneNumber;
    private String countryISO2;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public boolean isMan() {
        return isMan;
    }

    public void setMan(boolean man) {
        isMan = man;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getCountry() {
        Locale locale = new Locale("", countryISO2);
        return locale.getDisplayCountry();
    }

    public String getCountryISO2(){
        return countryISO2;
    }
    public void setCountryISO2(String countryCode) {
        this.countryISO2 = countryCode;
    }
}

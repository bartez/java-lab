package com.bartez.lab9.database;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PersonHelper extends SQLiteOpenHelper {

    private static final int DATABASE_VERSION = 1;

    public static final String PERSON_TABLE = "PERSON";
    public static final String COLUMN_ID = "id";
    public static final String COLUMN_NAME = "name";
    public static final String COLUMN_SURNAME = "surname";
    public static final String COLUMN_EMAIL = "email";
    public static final String COLUMN_PHONE = "phoneNumber";

    private static final String PERSON_TABLE_CREATE = "CREATE TABLE IF NOT EXISTS " + PERSON_TABLE +
            "(" + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " + COLUMN_NAME + " VARCHAR(50), " + COLUMN_SURNAME +
            " VARCHAR(100)," + COLUMN_EMAIL + " VARCHAR(150), " + COLUMN_PHONE + " VARCHAR(20) UNIQUE);";

    private static final String SEED_DATA = " INSERT INTO Person (name,surname,email,phoneNumber) " +
            "VALUES ('Bartosz','Stępień','bartosz.stepien@outlook.com','000 000 000');";

    public PersonHelper(Context context) {
        super(context, "BazaAdresowa", null, DATABASE_VERSION);
        Log.d("DB", "PersonHelper helper start");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(PERSON_TABLE_CREATE);
        sqLiteDatabase.execSQL(SEED_DATA);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {

    }
}

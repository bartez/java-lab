package com.bartez.lab9.helpers;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.FragmentManager;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import com.bartez.lab9.R;
import com.bartez.lab9.model.Person;

public class PersonFormDialog extends DialogFragment {



    public static interface OnCompleteListener {
        public abstract void onComplete(Person person);
    }

    private long id;
    private OnCompleteListener mListener;
    final private Person newPerson = new Person();

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.new_person_dialog, null);
        builder.setView(view);
        final EditText nameText = (EditText) view.findViewById(R.id.person_form_name);
        final EditText surnameText = (EditText) view.findViewById(R.id.person_form_surname);
        final EditText phoneText = (EditText) view.findViewById(R.id.person_form_tel);
        final EditText emailText = (EditText) view.findViewById(R.id.person_email);
        final EditText streetNoText = (EditText) view.findViewById(R.id.person_street_no);
        final EditText cityNoText = (EditText) view.findViewById(R.id.person_city_no);

        if(getArguments() != null){
            String name = getArguments().getString("person_name");
            String surname = getArguments().getString("person_surname");
            String email = getArguments().getString("person_email");
            String phone = getArguments().getString("person_phone");
            String street = getArguments().getString("person_street_no");
            String city = getArguments().getString("person_city_no");

            nameText.setText(name);
            surnameText.setText(surname);
            emailText.setText(email);
            phoneText.setText(phone);
            cityNoText.setText(city);
            streetNoText.setText(street);
        }

        builder.setPositiveButton(R.string.add, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                newPerson.setName(nameText.getText().toString());
                newPerson.setSurname(surnameText.getText().toString());
                newPerson.setPhoneNumber(phoneText.getText().toString());
                newPerson.setEmail(emailText.getText().toString());
                newPerson.setStreetNo(streetNoText.getText().toString());
                newPerson.setCityNo(cityNoText.getText().toString());
                if(getArguments() != null && getArguments().size() > 0){
                    long personId = getArguments().getLong("person_id");
                    if(personId > 0){
                        newPerson.setId(personId);
                    }
                }

                mListener.onComplete(newPerson);
            }
        });

        builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                PersonFormDialog.this.getDialog().cancel();
            }
        });

        return builder.create();
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            this.mListener = (OnCompleteListener)activity;
        }
        catch (final ClassCastException e) {
            throw new ClassCastException(activity.toString() + " must implement OnCompleteListener");
        }
    }
}

package com.bartez.lab9.database;


import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class PersonExtendedHelper extends SQLiteOpenHelper {

    private static int DATABASE_VERSION = 2;
    public static final String PERSON_TABLE = "PERSON";
    public static final String COLUMN_STREET_NO = "STREETNO";
    public static final String COLUMN_CITY_NO = "CITYNO";


    private static final String add_column_street_no = "ALTER TABLE " + PERSON_TABLE + " ADD COLUMN " + COLUMN_STREET_NO + " VARCHAR(10);";
    private static final String add_column_city_no = "ALTER TABLE " + PERSON_TABLE + " ADD COLUMN " + COLUMN_CITY_NO + " VARCHAR(10);";

    public PersonExtendedHelper(Context context) {
        super(context, "BazaAdresowa", null, DATABASE_VERSION);
        Log.d("DB", "PersonExtendedHelper helper start");
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        sqLiteDatabase.execSQL(add_column_street_no);
        sqLiteDatabase.execSQL(add_column_city_no);
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        onCreate(sqLiteDatabase);
    }
}

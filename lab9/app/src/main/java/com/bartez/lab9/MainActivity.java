package com.bartez.lab9;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.bartez.lab9.helpers.PersonAdapter;
import com.bartez.lab9.helpers.PersonFormDialog;
import com.bartez.lab9.model.Person;
import com.bartez.lab9.model.PersonDataSource;

public class MainActivity extends AppCompatActivity implements PersonFormDialog.OnCompleteListener{

    private ListView personsList;
    private Button newPersonButton;

    private PersonDataSource dataSource;
    private PersonAdapter personAdapter;
    final private PersonFormDialog dialog = new PersonFormDialog();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dataSource = new PersonDataSource(this);
        dataSource.open();

        personAdapter = new PersonAdapter(getApplicationContext(), dataSource, getSupportFragmentManager());
        personsList = (ListView)findViewById(R.id.persons_list);
        newPersonButton = (Button)findViewById(R.id.new_person_btn);

        Log.d("DB", "Liczba rekordow: " + dataSource.getAllPersons().size());
        dataSource.close();
        personsList.setAdapter(personAdapter);
        personsList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Person person = (Person) personAdapter.getItem(i);
                Bundle arguments = new Bundle();
                arguments.putLong("person_id", person.getId());
                arguments.putString("person_name", person.getName());
                arguments.putString("person_surname", person.getSurname());
                arguments.putString("person_email", person.getEmail());
                arguments.putString("person_phone", person.getPhoneNumber());
                arguments.putString("person_city_no", person.getCityNo());
                arguments.putString("person_street_no", person.getStreetNo());

                dialog.setArguments(arguments);
                dialog.show(getSupportFragmentManager(), "NEW_PERSON_FRAGMENT");

            }

        });
        newPersonButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.show(getSupportFragmentManager(), "NEW_PERSON_FRAGMENT");
            }
        });
    }

    @Override
    public void onComplete(Person person) {
        dataSource.open();
        if(person != null){
            if(person.getId() > 0){
                dataSource.update(person.getId(), person);
            }else {
                dataSource.insert(person);
            }
            personAdapter.reload();
        }
        dataSource.close();
    }
}

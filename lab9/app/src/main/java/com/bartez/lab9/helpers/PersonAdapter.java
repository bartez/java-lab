package com.bartez.lab9.helpers;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.bartez.lab9.R;
import com.bartez.lab9.model.Person;
import com.bartez.lab9.model.PersonDataSource;

import java.util.ArrayList;
import java.util.List;

public class PersonAdapter extends BaseAdapter {

    private Context context;
    private List<Person> persons = new ArrayList<>();
    private static LayoutInflater inflater;
    private PersonDataSource dataSource;
    private FragmentManager fragmentManager;

    public PersonAdapter(Context context, PersonDataSource dataSource, FragmentManager fragmentManager){
        this.context = context;
        this.dataSource = dataSource;
        this.fragmentManager = fragmentManager;
        inflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        reload();
    }
    @Override
    public int getCount() {
        return persons.size();
    }

    @Override
    public Object getItem(int i) {
        return persons.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(final int i, View view, ViewGroup viewGroup) {

        if(view == null){
            view = inflater.inflate(R.layout.person_row_layout, viewGroup, false);
        }

        final Person person = persons.get(i);
        TextView personName = (TextView)view.findViewById(R.id.person_name);
        TextView personTel = (TextView)view.findViewById(R.id.person_tel);
        TextView personEmail = (TextView)view.findViewById(R.id.person_email);
        TextView personCityNo = (TextView)view.findViewById(R.id.person_city_no);
        TextView personStreetNo = (TextView)view.findViewById(R.id.person_street_no);
        final Button personRemoveButton = (Button)view.findViewById(R.id.person_removeBtn);
        Button personEditButton = (Button)view.findViewById(R.id.person_editBtn);

        personRemoveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dataSource.open();
                removePerson(person);
                dataSource.close();
                Toast.makeText(context, "Person removed!", Toast.LENGTH_SHORT).show();
                reload();
            }
        });

        personEditButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                PersonFormDialog dialog = new PersonFormDialog();
                Bundle arguments = new Bundle();
                arguments.putLong("person_id", person.getId());
                arguments.putString("person_name", person.getName());
                arguments.putString("person_surname", person.getSurname());
                arguments.putString("person_email", person.getEmail());
                arguments.putString("person_phone", person.getPhoneNumber());
                arguments.putString("person_street_no", person.getStreetNo());
                arguments.putString("person_city_no", person.getCityNo());

                dialog.setArguments(arguments);
                dialog.show(fragmentManager, "NEW_PERSON_FRAGMENT");
            }
        });

        personName.setText(person.getName() + " " + person.getSurname());
        personTel.setText(person.getPhoneNumber());
        personEmail.setText(person.getEmail());
        personCityNo.setText(person.getCityNo());
        personStreetNo.setText(person.getStreetNo());
        return view;
    }

    private void removePerson(Person person){
        dataSource.delete(person);
    }

    public void reload(){
        dataSource.open();
        persons = dataSource.getAllPersons();
        notifyDataSetChanged();
        dataSource.close();
    }
}

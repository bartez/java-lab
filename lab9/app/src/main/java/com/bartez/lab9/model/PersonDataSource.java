package com.bartez.lab9.model;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import com.bartez.lab9.database.PersonExtendedHelper;
import com.bartez.lab9.database.PersonHelper;

import java.util.ArrayList;
import java.util.List;

public class PersonDataSource {

    private SQLiteDatabase database;
    private PersonHelper dbHelper;
    private PersonExtendedHelper dbExtendedHelper;

    public PersonDataSource(Context context){
        dbHelper = new PersonHelper(context);
        dbExtendedHelper = new PersonExtendedHelper(context);
    }

    public void open() throws SQLException {
        database = dbExtendedHelper.getWritableDatabase();
    }

    public void close() {
        dbExtendedHelper.close();
    }

    public void insert(Person person){
        ContentValues values = getContentValues(person);

        database.insert(dbHelper.PERSON_TABLE, null, values);
    }

    @NonNull
    private ContentValues getContentValues(Person person) {
        ContentValues values = new ContentValues();
        values.put(dbHelper.COLUMN_NAME, person.getName());
        values.put(dbHelper.COLUMN_SURNAME, person.getSurname());
        values.put(dbHelper.COLUMN_EMAIL, person.getEmail());
        values.put(dbHelper.COLUMN_PHONE, person.getPhoneNumber());
        values.put(dbHelper.COLUMN_PHONE, person.getPhoneNumber());
        values.put(dbHelper.COLUMN_PHONE, person.getPhoneNumber());
        values.put(dbExtendedHelper.COLUMN_STREET_NO, person.getStreetNo());
        values.put(dbExtendedHelper.COLUMN_CITY_NO, person.getCityNo());
        return values;
    }

    public void delete(Person person){
        long id = person.getId();
        Log.d("DB", "Person deleted with id: " + id);
        database.delete(dbHelper.PERSON_TABLE, dbHelper.COLUMN_ID + " = " + id, null);
    }

    public void update(long id, Person person){
        Log.d("DB", "Person modified with id: " + id);
        database.update(dbHelper.PERSON_TABLE, getContentValues(person), dbHelper.COLUMN_ID + " = " + id, null);
    }

    public List<Person> getAllPersons(){
        open();
        List<Person> persons = new ArrayList<>();
        final String columns [] = {dbHelper.COLUMN_ID, dbHelper.COLUMN_NAME, dbHelper.COLUMN_SURNAME, dbHelper.COLUMN_EMAIL, dbHelper.COLUMN_PHONE, dbExtendedHelper.COLUMN_CITY_NO, dbExtendedHelper.COLUMN_STREET_NO};
        Cursor cursor = database.query(dbHelper.PERSON_TABLE, columns, null, null, null, null, null);

        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Person person = getPersonFromCursor(cursor);
            persons.add(person);
            cursor.moveToNext();
        }
        // make sure to close the cursor
        cursor.close();
        close();
        return persons;
    }

    public Person getPersonFromCursor(Cursor cursor){
        Person person = new Person();
        person.setId(cursor.getLong(0));
        person.setName(cursor.getString(1));
        person.setSurname(cursor.getString(2));
        person.setEmail(cursor.getString(3));
        person.setPhoneNumber(cursor.getString(4));
        person.setCityNo(cursor.getString(5));
        person.setStreetNo(cursor.getString(6));

        return person;
    }
}

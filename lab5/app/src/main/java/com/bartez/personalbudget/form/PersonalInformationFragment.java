package com.bartez.personalbudget.form;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.bartez.personalbudget.R;
import com.bartez.personalbudget.model.Person;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link PersonalInformationFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link PersonalInformationFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class PersonalInformationFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private EditText nameText;
    private EditText surnameText;
    private EditText mailText;
    private Spinner sexSpinner;
    public PersonalInformationFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment PersonalInformationFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static PersonalInformationFragment newInstance() {
        PersonalInformationFragment fragment = new PersonalInformationFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view =  inflater.inflate(R.layout.fragment_personal_information, container, false);
        nameText = (EditText)view.findViewById(R.id.personal_name);
        surnameText = (EditText)view.findViewById(R.id.personal_surname);
        mailText = (EditText)view.findViewById(R.id.personal_email);
        sexSpinner = (Spinner)view.findViewById(R.id.sex_choice);

        Context ctx = view.getContext();
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(ctx,
                R.array.sex, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        sexSpinner.setAdapter(adapter);

        return view;
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }

    private String getName(){
        return nameText.getText().toString();
    }

    private String getSurname(){
        return surnameText.getText().toString();
    }

    private String getSex(){
        return sexSpinner.getSelectedItem().toString();
    }

    public Person getPerson(){
        return new Person(getName(), getSurname(), getSex());
    }
}

package com.bartez.personalbudget.form;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bartez.personalbudget.R;
import com.bartez.personalbudget.model.Person;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link ProfileCardFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link ProfileCardFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class ProfileCardFragment extends Fragment {

    private OnFragmentInteractionListener mListener;
    private TextView nameText ;
    private TextView surnameText;
    private TextView sexText;
    private ImageView sexImage;

    private String name;
    private String surname;
    private String sex;

    public ProfileCardFragment() {
        // Required empty public constructor
    }

    public Context getContext(){
        return getActivity().getApplicationContext();
    }
    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment ProfileCardFragment.
     */
    public static ProfileCardFragment newInstance() {
        ProfileCardFragment fragment = new ProfileCardFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile_options, container, false);
        InitializeControls(view);
        DeclarePerson(name, sex, surname);

        if(isInvalidWomanName()){
            Toast.makeText(getContext(), R.string.woman_alert, Toast.LENGTH_LONG).show();
        }

        return view;
    }

    private boolean isInvalidWomanName() {

        if(sex.equals("Woman") && !name.endsWith("a") || name.toLowerCase().equals("kuba")){
            return true;
        }
        return false;
    }

    private void DeclarePerson(String name, String sex, String surname) {
        nameText.setText(String.format(getContext().getString(R.string.name_label), name));
        sexText.setText(String.format(getContext().getString(R.string.sex_label), sex));
        surnameText.setText(String.format(getContext().getString(R.string.surname_label), surname));
        sexImage.setImageResource(sex.equals("Man") ? R.drawable.boss : R.drawable.woman);
    }

    private void InitializeControls(View view) {
        nameText = (TextView)view.findViewById(R.id.card_name);
        surnameText = (TextView)view.findViewById(R.id.card_surname);
        sexText = (TextView)view.findViewById(R.id.card_sex);
        sexImage = (ImageView)view.findViewById(R.id.sex_image);
    }

    // TODO: Rename method, update argument and hook method into UI event
    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof OnFragmentInteractionListener) {
            mListener = (OnFragmentInteractionListener) context;
        } else {
            throw new RuntimeException(context.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        void onFragmentInteraction(Uri uri);
    }

    public void setPerson(Person person){

        this.name = person.getName();
        this.surname = person.getSurname();
        this.sex = person.getSex();
    }
}

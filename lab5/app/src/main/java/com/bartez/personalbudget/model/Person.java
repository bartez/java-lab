package com.bartez.personalbudget.model;

/**
 * Created by Bartosz Stępień on 2016-11-21.
 */

public class Person {

    private String name;
    private String surname;
    private String sex;

    public Person(){}

    public Person(String name, String surname, String sex){
        this.name = name;
        this.surname = surname;
        this.sex = sex;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}

package com.bartez.personalbudget.form;

import android.app.FragmentTransaction;
import android.content.Context;
import android.net.Uri;

import android.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.bartez.personalbudget.R;
import com.bartez.personalbudget.model.Person;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements PersonalInformationFragment.OnFragmentInteractionListener, ProfileCardFragment.OnFragmentInteractionListener{

    final ArrayList<Fragment> fragmentSteps = new ArrayList<>();
    private Context context;
    private Button nextBtn;
    private Button backBtn;
    private Button finishBtn;

    private Person person;
    int step = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createFragment();
        replaceStepFragment(R.id.fragment_container, fragmentSteps.get(step));

        context = getApplicationContext();
        nextBtn = (Button)findViewById(R.id.next_button);
        backBtn = (Button)findViewById(R.id.back_button);
        finishBtn = (Button)findViewById(R.id.finish_button);


        nextBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nextStep();
            }
        });

        backBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                backStep();
            }
        });

        finishBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    public void createFragment(){
        fragmentSteps.add(PersonalInformationFragment.newInstance());
        fragmentSteps.add(ProfileCardFragment.newInstance());
    }

    public void finish(){

    }
    public void nextStep(){
        if(step >= fragmentSteps.size() -1){
            Toast maxStepToast = Toast.makeText(context, "End form", Toast.LENGTH_SHORT);
            maxStepToast.show();
            return;
        }

        Fragment actualFragment = fragmentSteps.get(step);
        step++;
        Fragment nextFragment = fragmentSteps.get(step);
        replaceStepFragment(R.id.fragment_container, nextFragment);

        if(nextFragment instanceof ProfileCardFragment && actualFragment instanceof PersonalInformationFragment){
            PersonalInformationFragment personalFragment = (PersonalInformationFragment) actualFragment;
            person = personalFragment.getPerson();

            ((ProfileCardFragment) nextFragment).setPerson(person);
        }

        if(!backBtn.isEnabled()){
            backBtn.setEnabled(true);
        }

        if(step >= fragmentSteps.size() - 1){
            nextBtn.setVisibility(View.GONE);
            finishBtn.setVisibility(View.VISIBLE);
        }
    }

    private void replaceStepFragment(int i, Fragment fragment) {
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(i, fragment);
        transaction.addToBackStack(null);
        transaction.commit();
    }

    public void backStep(){
        step--;
        replaceStepFragment(R.id.fragment_container, fragmentSteps.get(step));

        if(nextBtn.getVisibility() == View.GONE){
            nextBtn.setVisibility(View.VISIBLE);
            finishBtn.setVisibility(View.GONE);
        }

        if(step <= 0){
            backBtn.setEnabled(false);
        }
    }
    @Override
    public void onFragmentInteraction(Uri uri) {
        Toast.makeText(context, "Test", Toast.LENGTH_SHORT).show();
    }
}

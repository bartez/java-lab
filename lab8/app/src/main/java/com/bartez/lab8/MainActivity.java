package com.bartez.lab8;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        final LocationManager locationManager = (LocationManager) getApplicationContext().getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = new ArrayList<>();
        providers.add("none");
        providers.addAll(locationManager.getAllProviders());

        ArrayAdapter<String> providerAdapter = new ArrayAdapter<>(this, android.R.layout.simple_dropdown_item_1line, providers);
        final Spinner locationProvider = (Spinner) findViewById(R.id.provider_spinner);
        final TextView selectedProvider = (TextView) findViewById(R.id.selected_provider);
        final TextView actualLocation = (TextView) findViewById(R.id.actualLocation);
        final TextView connectionStatus = (TextView) findViewById(R.id.connectionType);
        final Button checkConnection = (Button) findViewById(R.id.checkConnection);
        final TextView wifiInfoText = (TextView) findViewById(R.id.wifi_info);

        locationProvider.setAdapter(providerAdapter);
        selectedProvider.setText(String.format(getString(R.string.selected_provider), locationProvider.getSelectedItem()));

        final ConnectivityManager cm = (ConnectivityManager)getApplicationContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        final LocationListener locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {
                actualLocation.setText(String.format(getString(R.string.actual_location), location.getLongitude(), location.getLatitude()));
            }

            @Override
            public void onStatusChanged(String s, int i, Bundle bundle) {

            }

            @Override
            public void onProviderEnabled(String s) {
                Toast.makeText(getApplicationContext(), "Zmieniono adapter na: " + s, Toast.LENGTH_SHORT).show();
            }

            @Override
            public void onProviderDisabled(String s) {

            }
        };

        final Activity myActivity = this;
        locationProvider.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedProvider.setText(String.format(getString(R.string.selected_provider), locationProvider.getSelectedItem()));
                if (ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(myActivity,new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION}, 1);
                    return;
                }

                if(locationProvider.getSelectedItem() != "none" ){
                    locationManager.requestLocationUpdates(locationProvider.getSelectedItem().toString(), 0, 0, locationListener);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {
                return;
            }
        });

        checkConnection.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                NetworkInfo networkInfo = cm.getActiveNetworkInfo();
                if(networkInfo != null){
                    connectionStatus.setText(String.format(getString(R.string.connection_type), networkInfo.getTypeName()));

                    if(networkInfo.getType() == ConnectivityManager.TYPE_WIFI){
                        final WifiManager wifiManager = (WifiManager) getApplicationContext().getSystemService(Context.WIFI_SERVICE);
                        final WifiInfo connectionInfo = wifiManager.getConnectionInfo();
                        wifiInfoText.setText("SSID: " + connectionInfo.getSSID() + "\nIP: " + connectionInfo.getIpAddress());
                    }
                }
            }
        });

    }
}
